import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TesteApiFormsPage } from './teste-api-forms';

@NgModule({
  declarations: [
    TesteApiFormsPage,
  ],
  imports: [
    IonicPageModule.forChild(TesteApiFormsPage),
  ],
})
export class TesteApiFormsPageModule {}
