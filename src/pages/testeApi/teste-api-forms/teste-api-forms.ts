import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ITesteApi } from '../testeApi.interface';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { TesteapiProvider } from '../../../providers/testeapi/testeapi';

@IonicPage()
@Component({
  selector: 'page-teste-api-forms',
  templateUrl: 'teste-api-forms.html',
})
export class TesteApiFormsPage {

  public testeApi: ITesteApi = {};
  public desativarBotao: boolean = false;
  public textoBotao: string = 'Salvar';
  public habilitarBotaoExcluir: boolean = false;
  private options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: true
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private camera: Camera,
              private testeApiprovider: TesteapiProvider) {
  }

  ionViewDidLoad() {
    this.testeApi = this.navParams.get('params');
    this.habilitarBotaoExcluir = this.testeApi.id ? true : false;
  }

  public excluirFoto(): void {
    this.testeApi.imagem = null;
  }

  private controlarBotao(realizarRequest: boolean): void  {
    this.textoBotao = realizarRequest ? 'Aguarde...' : 'Salvar';
    this.desativarBotao = realizarRequest;
  }

  public btnExcluir(): void { 
    this.testeApiprovider.delete(this.testeApi.id).subscribe(() => this.navCtrl.pop());
  }

  public btnSalvar(): void { 
    this.controlarBotao(true);
    if(this.testeApi.id) {
      this.alterarDados();
      return;
    }

    this.salvarDados();
  }

  private alterarDados(): void {
    this.testeApiprovider.update(this.testeApi).subscribe(() => this.navCtrl.pop());
  }

  private salvarDados(): void { 
    this.testeApiprovider.insert(this.testeApi).subscribe(()=> this.navCtrl.pop());
  }

  public tirarFoto(): void { 
    this.camera.getPicture(this.options).then((imageData) => {
      let base64 = 'data:image/jpeg;base64,' + imageData;
      this.testeApi.imagem = base64;
    },(error) => console.log(error))
    .catch((error) => console.log(error));
  }
}
