import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ITesteApi } from '../testeApi.interface';
import { TesteapiProvider } from '../../../providers/testeapi/testeapi';

@IonicPage()
@Component({
  selector: 'page-teste-api-details',
  templateUrl: 'teste-api-details.html',
})
export class TesteApiDetailsPage {

  public testeApiList: ITesteApi[] = []
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public testeApiProvider: TesteapiProvider) {
  }

  ionViewWillEnter() {
    this.testeApiProvider.list().subscribe((data: ITesteApi[]) => {
      this.testeApiList = data;
      console.log('List', this.testeApiList);
    }, () => {
      console.log('Aconteceu algum erro');
    });
  }

  public abrirForm(testeApiParam: ITesteApi): void  {
    if (testeApiParam) {
      this.navCtrl.push('TesteApiFormsPage', { params: testeApiParam});
      return;
    }

    const testeApi: ITesteApi = {};
    this.navCtrl.push('TesteApiFormsPage', { params: testeApi});
  }

}
