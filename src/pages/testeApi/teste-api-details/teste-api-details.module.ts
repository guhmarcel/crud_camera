import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TesteApiDetailsPage } from './teste-api-details';

@NgModule({
  declarations: [
    TesteApiDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TesteApiDetailsPage),
  ],
})
export class TesteApiDetailsPageModule {}
