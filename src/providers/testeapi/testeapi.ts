import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITesteApi } from '../../pages/testeApi/testeApi.interface';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TesteapiProvider {

  constructor(public http: HttpClient) {
    
  }

  private urlApi: string = 'https://backvideotest.azurewebsites.net/api/testeapi/';

  public list(): Observable<ITesteApi[]> {
    return this.http.get(this.urlApi).pipe(map((response: any) => this.mapTesteApi(response)));
  }

  public insert(body: any): Observable<any> { 
    return this.http.post(this.urlApi, body);
  }

  public update(body: any): Observable<any> {
    return this.http.put(this.urlApi, body);
  }

  public delete(id: string): Observable<any> { 
    return this.http.delete(`${this.urlApi}${id}`);
  }

  private mapTesteApi(response: any): ITesteApi[] {
    let testeApiList: ITesteApi[] = [];

    if (response) {
      response.forEach(data => {
        const testeApi: ITesteApi = {
          id: data.id,
          nome: data.nome,
          imagem: data.imagem
        }

        testeApiList.push(testeApi);
      });
    }

    return testeApiList;
  }
}
